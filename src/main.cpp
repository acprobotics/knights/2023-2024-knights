#include "main.h"
#include "EZ-Template/drive/drive.hpp"
#include "autons.hpp"

// Define the master controller:
pros::Controller master(pros::E_CONTROLLER_MASTER);
// Define the intake motor on port 3:
pros::Motor intakeMotor(10);
// Define the puncher motor on port 9:
pros::Motor puncherMotor(14);
// Define the ADI digital output for pistons on port 'g':
pros::ADIDigitalOut pistons('g');
// Define the ADI digital output for the hang mechanism on port 'h':
pros::ADIDigitalOut hangMech ('h');
// IMU
pros::Imu imu_sensor(8);


/*pros::Motor LFMotor(-18);
pros::Motor LBMotor(-19);
pros::Motor LMintakeMotor(-17);

pros::Motor RFMotor(13);
pros::Motor RBMotor(20);
pros::Motor RMMotor(2);

pros::Motor_Group allMotor ({LFMotor, LBMotor, LMintakeMotor, RFMotor, RBMotor, RMMotor});
pros::Motor_Group LMotor ({LFMotor, LBMotor, LMintakeMotor});
pros::Motor_Group RMotor ({RFMotor, RBMotor, RMMotor});*/



// Constants

const int DRIVE_SPEED = 110; // This is 110/127 (around 87% of max speed).  We don't suggest making this 127.
                             // If this is 127 and the robot tries to heading correct, it's only correcting by
                             // making one side slower.  When this is 87%, it's correcting by making one side
                             // faster and one side slower, giving better heading correction.
const int TURN_SPEED  = 90;
const int SWING_SPEED = 90;


/////
// For installation, upgrading, documentations and tutorials, check out our website!
// https://ez-robotics.github.io/EZ-Template/
/////


// Chassis constructor
Drive chassis (
  // Left Chassis Ports
  {-18, -19, -17} //{FRONT LEFT MOTOR, BACK LEFT MOTOR, MIDDLE LEFT}

  // Right Chassis Ports
  ,{13, 20, 2} //{FRONT RIGHT MOTOR, BACK RIGHT MOTOR, MIDDLE RIGHT}

  // IMU Port
  ,8

  // Wheel Diameter (Remember, 4" wheels without screw holes are actually 4.125!)
  ,3.25

  // Cartridge RPM
  ,600

  // External Gear Ratio (MUST BE DECIMAL) This is WHEEL GEAR / MOTOR GEAR
  // eg. if your drive is 84:36 where the 36t is powered, your RATIO would be 84/36 which is 2.333
  // eg. if your drive is 60:36 where the 36t is powered, your RATIO would be 60/36 which is 0.6
  // eg. if your drive is 36:60 where the 60t is powered, your RATIO would be 36/60 which is 0.6
  ,1.6667

);



/**
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */
void initialize() {
  // Print on brain
  pros::lcd::initialize();
  ez::ez_template_print();
  pros::lcd::set_background_color(0,0,0); //Black RGB
  pros::lcd::set_text_color(145,70,255); //Twitch Purple RGB
  
  pros::delay(500); // Stop the user from doing anything while legacy ports configure

  // Configure your chassis controls
  chassis.opcontrol_curve_buttons_toggle(false); // Enables modifying the controller curve with buttons on the joysticks
  chassis.opcontrol_drive_activebrake_set(0.1); // Sets the active brake kP. 
  chassis.opcontrol_curve_default_set(1, 1); //Sets curve (TANK/ARCADE CONTROL)
  default_constants(); //constants from autons.cpp

  // Autonomous Selector using LLEMU
  ez::as::auton_selector.autons_add({
    Auton("Example Drive\n\nDrive forward and come back.", drive_example),
    Auton("Example Turn\n\nTurn 3 times.", turn_example),
    Auton("Drive and Turn\n\nDrive forward, turn, come back. ", drive_and_turn),
    Auton("Drive and Turn\n\nSlow down during drive.", wait_until_change_speed),
    Auton("Swing Example\n\nSwing in an 'S' curve", swing_example),
    Auton("Combine all 3 movements", combining_movements),
    Auton("Interference\n\nAfter driving forward, robot performs differently if interfered or not.", interfered_example),
  });

  // Initialize chassis and auton selector
  chassis.initialize();
  ez::as::initialize();
  master.rumble(".");
  chassis.drive_imu_reset(); // Reset gyro position to 0
}



/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled() {
  // . . .
}



/**
 * Runs after initialize(), and before autonomous when connected to the Field
 * Management System or the VEX Competition Switch. This is intended for
 * competition-specific initialization routines, such as an autonomous selector
 * on the LCD.
 *
 * This task will exit when the robot is enabled and autonomous or opcontrol
 * starts.
 */
void competition_initialize() {
  // . . .
}



/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */
void autonomous() {
  chassis.pid_targets_reset(); // Resets PID targets to 0
  chassis.drive_imu_reset(); // Reset gyro position to 0
  chassis.drive_sensor_reset(); // Reset drive sensors to 0
  chassis.drive_brake_set(MOTOR_BRAKE_HOLD); // Set motors to hold.  This helps autonomous consistency

  ez::as::auton_selector.selected_auton_call(); // Calls selected auton from autonomous selector
}



/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */



//bool values for opcontrol()
bool isIntakeOn = false;
bool isIntakeReversed = true; // Flag to track the intake direction
bool isPuncherOn = false;
bool pistonWingsOpen = false; // Flag to track the piston wing state
bool hnagMechOpen = false; // Flag to track the piston wing state


void opcontrol() {
  // This is preference to what you like to drive on
  chassis.drive_brake_set(MOTOR_BRAKE_HOLD);
  master.rumble("."); // To check if the controller is ready

  
  while (true) {
    chassis.opcontrol_tank();

        // Toggle intake on/off with L1 button
        if (master.get_digital_new_press(DIGITAL_L2)) {
            isIntakeOn = !isIntakeOn;
            if (isIntakeOn) {
                intakeMotor.move(600);
            } else {
                intakeMotor.move(0);
            }
            pros::delay(20);
        }

        // Toggle reverse intake on/off with L2 button
        if (master.get_digital_new_press(DIGITAL_L1)) {
            isIntakeReversed = !isIntakeReversed;
            if (isIntakeReversed) {
                intakeMotor.move(-600);
            } else {
                intakeMotor.move(0);
            }
            pros::delay(20);
        }

        // Toggle intake on/off with R1 button
        if (master.get_digital_new_press(DIGITAL_R1)) {
            isPuncherOn = !isPuncherOn;
            if (isPuncherOn) {
                puncherMotor.move(100);
            } else {
                puncherMotor.move(0);
            }
            pros::delay(20);
        }

            if (master.get_digital_new_press(DIGITAL_A)) {
            pistonWingsOpen = !pistonWingsOpen;
            if (pistonWingsOpen) {
                pistons.set_value(1);
                pros::delay(50);
            } else {
                pistons.set_value(0);
                pros::delay(50);
            }
        }

        // Toggle catapult blocker with the A button
        if (master.get_digital_new_press(DIGITAL_B)) {
            hnagMechOpen = !hnagMechOpen;
            if (hnagMechOpen) {
                hangMech.set_value(1);
                pros::delay(50);
            } else {
                hangMech.set_value(0);
                pros::delay(50);
            }
        }



      if (master.get_digital_new_press(DIGITAL_LEFT)){
      autonomous();
      }

      if (master.get_digital_new_press(DIGITAL_RIGHT)){
      std::cout << "TEMPS: " 
            << chassis.right_motors[0].get_temperature() << " "
            << chassis.right_motors[1].get_temperature() << " "
            << chassis.right_motors[2].get_temperature() << " "
            << chassis.left_motors[0].get_temperature() << " "
            << chassis.left_motors[1].get_temperature() << " "
            << chassis.left_motors[2].get_temperature() << " "           
            << puncherMotor.get_temperature() << " "
            << intakeMotor.get_temperature() << std::endl;
      }



    pros::delay(50);
  }
}

